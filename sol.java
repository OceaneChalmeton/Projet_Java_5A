package gui;

import java.awt.*;
import javax.swing.ImageIcon;

public class sol extends Case{
	private boolean cible;

	//Déclaration des méthodes
	public sol(boolean cible,int x,int y) {
		super(x,y);
		this.cible=cible;
	}
	 
	boolean getCible() {
		return cible;
	}

	public void DessinerObjetGraphique(Graphics p, coord position){
	    coord c=getCoordonnees();
	    boolean cible=getCible();
	    //si cible est true alors on dessine une cible
	    if (cible){
	    	ImageIcon monImage = new ImageIcon(":/Images/cible.jpg");
   		  	p.drawImage(monImage.getImage(),position.y+c.y*100,position.x+c.x*100,100,100,null);
	    //sinon on dessine une case sol
	    }else{
	    	ImageIcon monImage = new ImageIcon(":/Images/sol.jpg");
   		  	p.drawImage(monImage.getImage(),position.y+c.y*100,position.x+c.x*100,100,100,null);
	   }
	}
	public int getType() {
		return 2;
	}

}