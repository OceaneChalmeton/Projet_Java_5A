package gui;

import java.awt.EventQueue;
import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

public class FenetrePrincipale extends JFrame {

	private MonPanel monPanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FenetrePrincipale frame = new FenetrePrincipale();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FenetrePrincipale() {
		monPanel= new MonPanel(this);
		this.setContentPane(monPanel);
		setTitle("SOKOBAN");
		java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((screenSize.width-640)/2, (screenSize.height-480)/2,640,480);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		monPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		monPanel.setLayout(null);
		
		JButton btnRinitialiser = new JButton("Réinitialiser");
		btnRinitialiser.setBounds(12, 83, 110, 27);
		monPanel.add(btnRinitialiser);
		
		JButton btnAide = new JButton("Aide");
		btnAide.setBounds(12, 123, 110, 27);
		monPanel.add(btnAide);
		
		JButton btnQuitter = new JButton("Quitter");
		btnQuitter.setBounds(12, 162, 110, 27);
		monPanel.add(btnQuitter);
	}
	
	public void dessiner(Graphics g){
		Graphics bufferGraphics;
		Image offscreen;
		// On crée une image en mémoire de la taille du ContentPane
		offscreen = createImage(this.getContentPane().getWidth(),this.getContentPane().getHeight());
		// On récupère l'objet de type Graphics permettant de dessiner dans cette image
		bufferGraphics = offscreen.getGraphics();
		// On colore le fond de l'image en blanc
		bufferGraphics.setColor(Color.WHITE);
		bufferGraphics.fillRect(0,0,this.getContentPane().getWidth(),
		this.getContentPane().getHeight());
		// On dessine les objets graphiques de la liste dans l'image en mémoire pour éviter les
		// problèmes de scintillements
		
		
		mur m =new mur(1,1); 
		coord pos= new coord(50,10);
		m.DessinerObjetGraphique(bufferGraphics,pos);
			// On afficher l'image mémoire à l'écran
		g.drawImage(offscreen,0,0,null);
	}
}
