package gui;

public abstract class Case extends Fixe{
	
	public Case() {}

	public Case(int x, int y) {
		super(x,y);
	}
	 
	public abstract int getType();
}