package gui;

import java.util.*;
import java.awt.*;
import java.io.*;
import java.nio.file.*;

public class niveau {
	protected int largeur;
	protected int hauteur;
	protected int monNiveau;
	protected int nbCaisse;
	protected int nbCibleCouverte;

	protected coord position;
	protected ArrayList<sol> listeCible = new ArrayList<sol>();
	protected ArrayList<ArrayList<Case>> grille = new ArrayList<ArrayList<Case>>();
	protected ArrayList<caisse> listeCaisse = new ArrayList<caisse>();
	protected personnage Personnage = new personnage();
	protected int nbpas;

	//Déclaration des méthodes
	//Constructeur
	public niveau(String file){
	    char cas;
	    String ligne;
	    Path chemin = Paths.get(file);

	    //ouverture du fichier en lecture
	    InputStream fichier= Files.newInputStream(chemin);
	    //si l'ouverture est possible
	    if(fichier != null){
	    	BufferedReader reader= new BufferedReader(new InputStreamReader(fichier));
	        //tant qu'on est pas a la fin du fichier
	        while((ligne = reader.readLine())!=null){
	            //lecture des parammètres du niveau
	        	monNiveau= reader.read();
	        	hauteur= reader.read();
	        	largeur= reader.read();
	        	nbCaisse= reader.read();
	           
	            nbCibleCouverte=0;

	            //On fixe les paramètres pour savoir ou on dessine la grille
	         /*   QScreen *screen = QGuiApplication::primaryScreen();
	            const int screenWidth  = screen->size().width();
	            const int screenHeight = screen->size().height();
	            position.y=int((screenWidth-100*largeur)/2);
	            position.x=int((screenHeight-100*hauteur)/2);*/

	            //parcours de la grille
	            for(int i=0;i<hauteur;i++){
	                //vecteur temporaire de case
	                ArrayList<Case> temp;
	                for(int j=0;j<largeur;j++){
	                    //lecture du symbole
	                    cas=reader.readLine();
	                    switch (cas) {
	                        //mur
	                        case '#' : temp.push_back(new Mur(i,j));
	                                   break;
	                        //sol
	                        case '_' : temp.push_back(new sol(false,i,j));
	                                   break;
	                        //cible
	                        case '@' : temp.push_back(new sol(true,i,j));
	                                    //ajout de la cible dans la liste de cible
	                                   listeCible.push_back(new sol(true,i,j));
	                                   break;
	                        //caisse sur cible
	                        case '*' : temp.push_back(new sol(true,i,j));
	                                    //ajout de la cible dans la liste de cible
	                                   listeCible.push_back(new sol(true,i,j));
	                                   //ajout de la caisse dans la liste de caisse
	                                   listeCaisse.push_back(new Caisse(i,j));
	                                   //incrémentation du nombre de cibles couvertes
	                                   nbCibleCouverte++;
	                                   break;
	                        //caisse
	                        case '+' : temp.push_back(new sol(false,i,j));
	                                    //ajout de la caisse dans la liste de caisse
	                                   listeCaisse.push_back(new Caisse(i,j));                                   
	                                   break;
	                        //personnage
	                        case '$' : temp.push_back(new sol(false,i,j));
	                                   Personnage = new personnage(i,j);
	                                   //PersoInitiale = new personnage(i,j);
	                                   break;
	                        //personnage sur une cible
	                        case '%' : temp.push_back(new sol(true,i,j));
	                                   //ajout de la cible dans la liste de cible
	                                   listeCible.push_back(new sol(true,i,j));
	                                   Personnage = new personnage(i,j);                                  
	                                   break;
	                    }
	                }
	                //ajout de la variable temporaire dans la grille
	                grille.push_back(temp);
	            }
	        }
	    }
	    else {
	        System.out.println( "Impossible d'ouvrir le fichier");
	    }
	    //femeture du fichier
	    fichier.close();
	    //initialisation du nombre de pas
	    nbpas=0;
	}
	   
	public void DeplacerPersonnage(coord d){
	    coord cP=Personnage.getCoordonnees();
	    int type=grille.get(cP.x+d.x).get(cP.y+d.y).getType();
	    //si la case sur laquelle veut se déplacer le personnage est une case sol
	    if (type==2){
	        coord c = new coord();
	        c.x=cP.x+d.x;
	        c.y=cP.y+d.y;
	        //si il y a une caisse à pousser
	        if (existeDansListeCaisse(c)){
	            pousserCaisse(c,d);
	        }
	        //sinon on déplace seulement le personnage
	        else{
	            Personnage.updateCoordonnees(d);
	            //on incrémente le nombre de pas
	            nbpas+=1;
	        }
	    }
	}
	
	public void pousserCaisse(coord c1, coord d){
	    coord c = new coord();
	    c.x=c1.x+d.x;
	    c.y=c1.y+d.y;
	    int type=grille.get(c.x).get(c.y).getType();
	    //si c'est une case sol et qu'il n'y a pas de caisse à l'endroit où l'on veut pousser caisse
	    if (type==2 &&  !existeDansListeCaisse(c)){
	        //on update le perosnnage et on incrémente le nombre de pas
	        Personnage.updateCoordonnees(d);
	        nbpas+=1;
	        //parcourt de la liste de caisse
	        for(int i=0;i<nbCaisse;i++){
	            coord co=listeCaisse.get(i).getCoordonnees();
	            if((co.x==c1.x)&&(co.y==c1.y)){
	                //si la caisse était sur une cible
	                if(existeDansListeCible(co)){
	                    //comme on décale la caisse il y a une cible en moins
	                    nbCibleCouverte--;
	                }
	                //update des coordoonnées de la caisse
	                listeCaisse.get(i).updateCoordonnees(d);
	                c=listeCaisse.get(i).getCoordonnees();
	                //si on deplace une caisse sur une cible on incremente le nombre de cible
	                if(existeDansListeCaisse(co)){
	                    nbCibleCouverte++;
	                }
	            }
	        }
	    }
	}
	    
	 public personnage getPersonage() {
		 return Personnage;
	 }
	 
    
	 public boolean existeDansListeCible(coord c) {
	    boolean test = false;
	    //on parcourt la liste de cible
	    for (int i=0;i<nbCaisse;i++){
	    	coord c1 = listeCible.get(i).getCoordonnees();
	    	//si la caisse fait partie de la liste on renvoie vrai
	    	 if ((c1.x==c.x)&&(c1.y==c.y)){
	    		 test=true;
	    	  }
	    }
	    return test;
	 }
	 
	 public boolean existeDansListeCaisse(coord c){
		    boolean test = false;
		    //on parcourt la liste de caisse
		    for (int i=0;i<nbCaisse;i++){
		        coord c1 = listeCaisse.get(i).getCoordonnees();
		        //si la caisse fait partie de la liste on renvoie vrai
		        if ((c1.x==c.x)&&(c1.y==c.y)){
		            test=true;
		        }
		    }
		    return test;
		}

	 
	public niveau reinitialiser(){
	    //on supprime tous les paramètres
	    Personnage=null;
	    for (int i =0; i< (int)listeCible.size();i++){
	         listeCible.remove(i);
	    }
	    listeCible.clear();

	    for (int i =0; i<(int) listeCaisse.size();i++){
	         listeCaisse.remove(i);
	    }
	    listeCaisse.clear();

	    for (int i =0; i< hauteur;i++){
	        for (int j =0; j< largeur;j++){
	            grille.remove(i).remove(j);
	        }
	    }
	    grille.clear();
	    nbpas=0;
	    
	    String lienNiv = "/Polytech/camorel/Telechargements/ProjetPOO";
	    //on relit le fichier correspondant au niveau que l'on veut réinitialiser
	    String s=lienNiv +"/Niveau "+ Integer.toString(monNiveau);
	    s=s+".txt";
	    return new niveau(s);

	}
	 
	 public void afficherNiveau(Graphics p){
		    //parcourt de la grille
		    for(int i=0; i<hauteur; i++){
		        for(int j=0; j<largeur; j++){
		            grille.get(i).get(j).DessinerObjetGraphique(p,position);
		        }
		    }
		}

	    
	  public void afficherPersonnage(Graphics p) {
		  Personnage.DessinerObjetGraphique(p, position);
	  }
	  
	  public void afficherCaisse(Graphics p){
		    for(int i=0; i<nbCaisse;i++){
		        coord c = listeCaisse.get(i).getCoordonnees();
		        boolean test=existeDansListeCible(c);
		        //si la caisses est sur une cible on affiche une caisse "cible" sur la position
		        if(test){
		            listeCaisse.get(i).DessinerObjetGraphique(p,1,position);
		        //sinon on affiche une caisse sur la position
		        }else{
		            listeCaisse.get(i).DessinerObjetGraphique(p,0,position);
		        }

		    }
		}
	    
	    public int get_largeur(){
	        return largeur;
	    }
	    public int get_hauteur(){
	        return hauteur;
	    }
	 
	   public niveau niveauSuivant(){
		    //suppression de tous les paramètres
		    Personnage=null;
		    for (int i =0; i< (int)listeCible.size();i++){
		         listeCible.remove(i);
		    }
		    listeCible.clear();

		    for (int i =0; i<(int) listeCaisse.size();i++){
		         listeCaisse.remove(i);
		    }
		    listeCaisse.clear();

		    for (int i =0; i< hauteur;i++){
		        for (int j =0; j< largeur;j++){
		            grille.remove(i).remove(j);
		        }
		    }
		    grille.clear();

		    //incrémentation du niveau
		    monNiveau+=1;
		    String lienNiv ="/Polytech/camorel/Telechargements/ProjetPOO";
		    //lecture du fichier correspondant au niveau suivant
		    String s=lienNiv+"/Niveau "+  Integer.toString(monNiveau);
		    s=s+".txt";
		    return new niveau(s);
		}
	   
	   public boolean testVictoire(){
	        int i=0;
	        boolean test=true;
	        //parcourt de la liste de caisse, si toutes les coordonnées des caisses correspondent aux coordonnées de la liste de cible
	        while(i<nbCaisse && test==true){
	            coord c = listeCaisse.get(i).getCoordonnees();
	            test=existeDansListeCible(c);
	            i=i+1;
	        }
	        return test;
	    }
	    
	    public int get_niveau() {
	    	return monNiveau;
	    }
	    
	    public int get_pas() {
	    	return nbpas;
	    }
}