package gui;

import javax.swing.ImageIcon; 
import java.awt.*;

public abstract class ObjetGraphique {
	//Déclaration des constantes
	final String lienNiveau="/Polytech/camorel/Telechargements/ProjetPOO/";
	final String lienNiv="/Polytech/camorel/Telechargements/ProjetPOO";

	//structure de coordonnées
	  
	/*public class coord{
	  public int x=0;
	  public int y=0;
	}; */
	
	//Déclaration des attributs
	protected coord c= new coord();
	
	//Déclaration des méthodes
	public ObjetGraphique() {}
	//constructeur par défaut
	public ObjetGraphique(int x, int y) {
		c.majcoord(x, y);
	}
	
	 /*public ObjetGraphique( ObjetGraphique o){
		 c.x=o.c.x;
		 c.y=o.c.y;
	 }*/
	 
	 public coord getCoordonnees() {
		 return c;
	 }
	 
	 public abstract void DessinerObjetGraphique(Graphics p, coord position);

}
