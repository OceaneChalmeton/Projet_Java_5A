package gui;

import javax.swing.ImageIcon;
import java.awt.*;

public class personnage extends mobile {
	public personnage() {}
	public  personnage(int x, int y) {
		super(x,y);
	}
	  
	  //dessin de l'objet graphique personnage
	 public void DessinerObjetGraphique(Graphics p, coord position){
	        coord c=getCoordonnees();
	        ImageIcon monImage = new ImageIcon(":/Images/personnage.jpg");
			  p.drawImage(monImage.getImage(),position.y+c.y*100,position.x+c.x*100,100,100,null);
	  }


}